from django.contrib import admin
from .models import Card, Crafted, Recipe

class CardAdmin(admin.ModelAdmin):
    fields = ['name', 'serie', 'obtention', 'icon']
    search_fields = ['name', 'serie']
    list_display = ['name', 'serie', 'obtention', 'admin_image']

class CraftedAdmin(admin.ModelAdmin):
    fields = ['name', 'level', 'url', 'img_url', 'craft_type']
    search_fields = ['name']
    list_display = ['name', 'level', 'url', 'img_url', 'craft_type']

class RecipeAdmin(admin.ModelAdmin):
    autocomplete_fields = ['card1', 'card2', 'card3', 'card4', 'card5', 'craft']

admin.site.register(Card, CardAdmin)
admin.site.register(Crafted, CraftedAdmin)
admin.site.register(Recipe, RecipeAdmin)