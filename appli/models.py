from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.core.files import File
from urllib.request import urlopen, Request
from tempfile import NamedTemporaryFile
from django.utils.safestring import mark_safe
from .scrapper_util import *
import time
def update_filename(instance, filename):
    path = "appli/static/images/cards"
    return '{0}/{1}.svg'.format(path, instance.pk)

class Card(models.Model):
    name = models.CharField(max_length=200)
    serie = models.IntegerField()
    obtention = models.CharField(max_length=1500)
    icon = models.ImageField(upload_to=update_filename)
    icon_url = models.URLField()

    def getIconName(self):
        return self.icon.url.split("/")[-1]
    def __str__(self):
        return self.name

    def clean(self):
        cleaned_data = super(Card, self).clean()
        if Card.objects.filter(name=self.name):
            raise ValidationError(
                    "Cette carte existe déjà"
                )

    def admin_image(self):
        src = 'https://www.temporiziovo.fr/static/images/cards/' + str(self.pk) + '.svg'
        return mark_safe('<img src="{}" width="100" />'.format(src))
    admin_image.short_description = 'Image'
    admin_image.allow_tags = True

    #def get_remote_image(self):
     #   if self.icon_url and not self.icon:
      #      img_temp = NamedTemporaryFile(delete=True)
       #     img_temp.write(urlopen(self.icon_url).read())
        #    img_temp.flush()
         #   self.icon.save(f"image_{self.pk}", File(img_temp))
        #self.save()

def update_filename_crafted(instance, filename):
    path = "appli/static/images/crafted"
    return '{0}/{1}.png'.format(path, instance.pk)


class Crafted(models.Model):
    name = models.CharField(max_length=200)
    level = models.IntegerField()
    url = models.CharField(max_length=2000)
    img_url = models.CharField(max_length=2000)
    img = models.ImageField(upload_to=update_filename_crafted)
    craft_type = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name

    def getIconName(self):
        return self.img.url.split("/")[-1]

    #def get_remote_image(self):
        #if self.img_url and not self.img:
            #USER_AGENT = GetHeader()
            #req = Request(self.img_url, headers={'User-Agent': USER_AGENT})
            #time.sleep(random.random())
            #img_temp = NamedTemporaryFile(delete=True)
            #img_temp.write(urlopen(req).read())
            #img_temp.flush()
            #self.img.save(f"image_{self.pk}", File(img_temp))
        #self.save()

class Recipe(models.Model):
    card1 = models.ForeignKey(Card, on_delete=models.CASCADE, related_name='card1')
    card2 = models.ForeignKey(Card, on_delete=models.CASCADE, related_name='card2')
    card3 = models.ForeignKey(Card, on_delete=models.CASCADE, related_name='card3')
    card4 = models.ForeignKey(Card, on_delete=models.CASCADE, related_name='card4')
    card5 = models.ForeignKey(Card, on_delete=models.CASCADE, related_name='card5')
    craft = models.ForeignKey(Crafted, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.craft.name + str(self.pk)


    def clean(self):
        cleaned_data = super(Recipe, self).clean()
        orderedId = [self.card1, self.card2, self.card3, self.card4, self.card5]
        orderedId.sort(key=lambda card: card.pk)
        
        if orderedId[0] == orderedId[1] or orderedId[1] == orderedId[2] or orderedId[2] == orderedId[3] or orderedId[3] == orderedId[4]:
            raise ValidationError(
                "Il y a des doublons"
                )

        self.card1 = orderedId[0]
        self.card2 = orderedId[1]
        self.card3 = orderedId[2]
        self.card4 = orderedId[3]
        self.card5 = orderedId[4]

        matching_data = Recipe.objects.filter(card1__pk=self.card1.pk)
        matching_data = matching_data.filter(card2__pk=self.card2.pk)
        matching_data = matching_data.filter(card3__pk=self.card3.pk)
        matching_data = matching_data.filter(card4__pk=self.card4.pk)
        matching_data = matching_data.filter(card5__pk=self.card5.pk)

        if matching_data.count() > 0:
            raise ValidationError(
                "Cette recette existe déjà, pour réaliser : " + str(matching_data.first().craft.name)
                )
