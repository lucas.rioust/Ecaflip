from django.urls import path


from . import views

app_name = 'appli'
urlpatterns = [
    path('', views.CardsView.as_view(), name='cards'),
    path('cards', views.CardsView.as_view(), name='cards'),
    path('card/<int:pk>', views.CardView.as_view(), name='card'),
    path('crafted/<int:pk>', views.CraftedView.as_view(), name='craft'),
    path('series/', views.SeriesView.as_view(), name='series'),
    path('series/<int:serieNumber>', views.SeriesView.as_view(), name='serie'),
    path('serieFilter', views.serieFilter, name='serieFilter'),
    path('combinations/', views.RecipesCombination.as_view(), name='combinations'),
    path('cardsCombination', views.cardsCombination, name='cardsCombination'),
    path('recipes/', views.RecipesView.as_view(), name='recipes'),
    path('recipes/<int:card1>', views.RecipesView.as_view(), name='recipes'),
    path('recipes/<int:card1>/<int:card2>', views.RecipesView.as_view(), name='recipes'),
    path('recipes/<int:card1>/<int:card2>/<int:card3>', views.RecipesView.as_view(), name='recipes'),
    path('recipes/<int:card1>/<int:card2>/<int:card3>/<int:card4>', views.RecipesView.as_view(), name='recipes'),
    path('recipes/<int:card1>/<int:card2>/<int:card3>/<int:card4>/<int:card5>', views.RecipesView.as_view(), name='recipes'),
]