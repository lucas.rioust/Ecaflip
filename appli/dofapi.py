import requests
import json
from types import SimpleNamespace
from .models import Crafted
import time
"""
"""
def fetch_dofapi():
    types = ["equipments", "idols", "consumables", "weapons"]
    url = "https://fr.dofus.dofapi.fr/"
    res = []

    for t in types :
        response = requests.get(url+t)
        res += [{"name": e["name"],
            "level": e["level"],
            "url": e["url"],
            "imgUrl": e["imgUrl"],
            "type": t[:-1]} 
            for e in response.json()]

    print(len(res))
    print(res[1])

    print(res[1]["name"])

    for i in range(0,len(res)):
        craft = Crafted(
            name=res[i]["name"],
            level=res[i]["level"],
            url=res[i]["url"],
            img_url=res[i]["imgUrl"],
            craft_type=res[i]["type"]
        )
        craft.save()

def update_urls():
    collection = Crafted.objects.all()
    for c in collection:
        url = c.img_url
        c.img_url = url[0:8]+url[25:]
        c.save()

def update_img():
    collection = Crafted.objects.all()
    for c in collection:
        if not c.img:
            print(c.pk)