from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.views import generic
from django.core.paginator import Paginator
from django.views.generic.list import MultipleObjectMixin
from django.core.files import File
import os
import urllib.request
from dal import autocomplete

import requests
from bs4 import BeautifulSoup

from .models import Card, Crafted, Recipe
from .dofapi import *


def InitBdd():
    base_url = 'https://tempocraft.altasia.fr/assets/images/cards/224'
    for i in range (1, 655):
        url = base_url
        if i < 10:
            url += "00"
        elif i < 100:
            url += "0"
        url += str(i) + ('.svg')

        name = "?-" + str(i)

        description = "? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?"

        serie = 201

        card = Card(name=name, serie=serie, obtention=description, icon_url = url)

        card.save()
        #card.get_remote_image()

def RenameBdd():
    base_url = "https://tempocraft.altasia.fr/card/224"
    for i in range (1, 655):
        URL = base_url
        if i < 10:
            URL += "00"
        elif i < 100:
            URL += "0"
        URL += str(i)

        id = i + 20

        card = Card.objects.filter(pk=id).first()
        page = requests.get(URL)
        soup = BeautifulSoup(page.content,"html.parser")
        card.name = '???' + soup.find(class_="jumbotron").h1.get_text()
        card.save()

def PopulateRecipe():
    for i in range (1,8000):
        cards = Card.objects.order_by('?')[:5].all()
        cards = list(cards)
        cards.sort(key=lambda card: card.pk)
        card1 = cards[0]
        card2 = cards[1]
        card3 = cards[2]
        card4 = cards[3]
        card5 = cards[4]
        matching_data = Recipe.objects.filter(card1__pk=card1.pk)
        matching_data = matching_data.filter(card2__pk=card2.pk)
        matching_data = matching_data.filter(card3__pk=card3.pk)
        matching_data = matching_data.filter(card4__pk=card4.pk)
        matching_data = matching_data.filter(card5__pk=card5.pk)

        if matching_data.count() == 0:
            crafted = Crafted.objects.order_by('?').first()
            recipe = Recipe(card1=card1, card2=card2, card3=card3, card4=card4, card5=card5, craft=crafted)
            recipe.save()


class CardsView(generic.ListView):
    template_name = 'card_list_big.html'
    paginate_by = 30
    def get_queryset(self):
        if 'serieNumber' in self.kwargs:
            return Card.objects.filter(serie=self.kwargs.get("serieNumber"))
        return Card.objects.order_by('serie')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["AutocompleteCards"] = Card.objects.all()
        context["AutocompleteCrafts"] = Crafted.objects.all()
        return context

class SeriesView(generic.ListView):
    template_name = 'card_list_big.html'
    paginate_by = 30
    def get_queryset(self):
        if 'serieNumber' in self.kwargs:
            return Card.objects.filter(serie=self.kwargs.get("serieNumber"))
        return Card.objects.order_by('serie')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["AutocompleteCards"] = Card.objects.all()
        context["AutocompleteCrafts"] = Crafted.objects.all()
        return context

class RecipesCombination(generic.TemplateView):
    template_name = "combination.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["AutocompleteCards"] = Card.objects.all()
        context["AutocompleteCrafts"] = Crafted.objects.all()
        return context

class RecipesView(generic.ListView):
    paginate_by = 10
    template_name = 'recipe_list.html'

    def get_queryset(self):
        recipes = Recipe.objects.all()
        cards = []
        if 'card1' in self.kwargs:
            cards.append(self.kwargs.get('card1'))
        if 'card2' in self.kwargs:
            cards.append(self.kwargs.get('card2'))
        if 'card3' in self.kwargs:
            cards.append(self.kwargs.get('card3'))
        if 'card4' in self.kwargs:
            cards.append(self.kwargs.get('card4'))
        if 'card5' in self.kwargs:
            cards.append(self.kwargs.get('card5'))

        count = len(cards)
        if count == 0:
            return recipes

        for card_pk in cards:
            previous = recipes
            recipes = previous.filter(card1__pk=card_pk)
            recipes2 = previous.filter(card2__pk=card_pk)
            recipes3 = previous.filter(card3__pk=card_pk)
            recipes4 = previous.filter(card4__pk=card_pk)
            recipes5 = previous.filter(card5__pk=card_pk)

            recipes = recipes | recipes2 | recipes3 | recipes4 | recipes5

        return recipes
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["AutocompleteCards"] = Card.objects.all()
        context["AutocompleteCrafts"] = Crafted.objects.all()
        return context


class CardView(generic.DetailView):
    template_name = 'card_detail.html'
    model = Card

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["AutocompleteCards"] = Card.objects.all()
        context["AutocompleteCrafts"] = Crafted.objects.all()
        recipes = Recipe.objects.all()
        pk = kwargs['object'].pk
        containedRecipes = recipes.filter(card1__pk=pk)
        containedRecipes = containedRecipes.union(recipes.filter(card2__pk=pk))
        containedRecipes = containedRecipes.union(recipes.filter(card3__pk=pk))
        containedRecipes = containedRecipes.union(recipes.filter(card4__pk=pk))
        containedRecipes = containedRecipes.union(recipes.filter(card5__pk=pk))
        containedRecipes = containedRecipes.union(recipes.filter(craft__pk=pk))
        paginator = Paginator(containedRecipes.order_by('craft'), 5)
        page = self.request.GET.get('page')
        linkedRecipes = paginator.get_page(page)
        context["page_obj"] = linkedRecipes
        return context

class CraftedView(generic.DetailView):
    template_name = 'craft_detail.html'
    model = Crafted
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["AutocompleteCards"] = Card.objects.all()
        context["AutocompleteCrafts"] = Crafted.objects.all()
        recipes = Recipe.objects.all()
        pk = kwargs['object'].pk
        containedRecipes = recipes.filter(craft=pk)
        context["LinkedRecipes"] = containedRecipes.order_by('craft')
        return context
    

"""
Fetch database from dofapi
"""


def serieFilter(request):
    if request.method == 'POST':
        return HttpResponseRedirect(reverse('appli:serie', kwargs={'serieNumber' : request.POST['choice']}))
    else:
        return HttpResponseRedirect(reverse('appli:series'))


def cardsCombination(request):
    if request.method == 'GET':
        return HttpResponseRedirect(reverse('appli:cards'))

    cards = []
    if 'card1' in request.POST and int(request.POST['card1']) >= 0:
        cards.append(request.POST['card1'])
    if 'card2' in request.POST and int(request.POST['card2']) >= 0:
        cards.append(request.POST['card2'])
    if 'card3' in request.POST and int(request.POST['card3']) >= 0:
        cards.append(request.POST['card3'])
    if 'card4' in request.POST and int(request.POST['card4']) >= 0:
        cards.append(request.POST['card4'])
    if 'card5' in request.POST and int(request.POST['card5']) >= 0:
        cards.append(request.POST['card5'])

    if len(cards) == 0:
        return HttpResponseRedirect(reverse('appli:combinations'))
    if len(cards) == 1:
        return HttpResponseRedirect(reverse('appli:card',  kwargs={'pk': cards[0]}))
    cards.sort()

    kwargs={}
    if len(cards) > 0:
        kwargs['card1'] = cards[0]
    if len(cards) > 1:
        kwargs['card2'] = cards[1]
    if len(cards) > 2:
        kwargs['card3'] = cards[2]
    if len(cards) > 3:
        kwargs['card4'] = cards[3]
    if len(cards) > 4:
        kwargs['card5'] = cards[4]

    
    return HttpResponseRedirect(reverse('appli:recipes', kwargs=kwargs))
    
