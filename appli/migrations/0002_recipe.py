# Generated by Django 3.1.7 on 2021-04-02 14:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('appli', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('card1', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='card1', to='appli.card')),
                ('card2', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='card2', to='appli.card')),
                ('card3', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='card3', to='appli.card')),
                ('card4', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='card4', to='appli.card')),
                ('card5', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='card5', to='appli.card')),
                ('craft', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appli.crafted')),
            ],
        ),
    ]
