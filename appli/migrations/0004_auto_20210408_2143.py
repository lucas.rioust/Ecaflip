# Generated by Django 3.2 on 2021-04-08 19:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appli', '0003_card_icon_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='crafted',
            name='craft_type',
            field=models.CharField(default='none', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='crafted',
            name='img_url',
            field=models.CharField(default='https://s.ankama.com/www/static.ankama.com/dofus-touch/www/game/items/200/6721.png', max_length=2000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='crafted',
            name='level',
            field=models.IntegerField(default=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='crafted',
            name='url',
            field=models.CharField(default='\x16https://www.dofus-touch.com/fr/mmorpg/encyclopedie/armes/14084-epee-granduk', max_length=2000),
            preserve_default=False,
        ),
    ]
